<?php

/**
 * @file
 * Enables modules and site configuration for a touchstone site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function touchstone_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'touchstone_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function touchstone_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}

/**
 * Alter metatags on the home so the site url is used instead of the node url.
 *
 * The home page is a node so we would like to use the metatag defaults for nodes.
 * Therefore, we disabled the frontage metatag defaults. If we don't alter the
 * url metatags, though, they will show the alias for the homepage node rather than
 * the url of the site.
 */
function touchstone_metatags_alter(array &$metatags, array &$context) {
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $metatags['canonical_url'] = '[site:url]';
    $metatags['og_url'] = '[site:url]';
    $metatags['og_type'] = 'website';
  }
}
